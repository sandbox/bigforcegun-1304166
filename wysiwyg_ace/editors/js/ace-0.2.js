(function($) {

    Drupal.wysiwyg.ace = {}; // ace instances storage

    /**
     * Attach this editor to a target element.
     *
     * See Drupal.wysiwyg.editor.attach.none() for a full desciption of this hook.
     */
    Drupal.wysiwyg.editor.attach.ace = function(context, params, settings) {
	if(typeof params != 'undefined') {
            console.log(params,settings);
            Drupal.wysiwyg.ace[params.field]={};

            //hide the textarea
            var $field=$('#'+params.field);
            var aceDivId=params.field+'-ace';
            var aceDivHtml='<div class="ace ' + aceDivId + '" id="' + aceDivId + '"></div>';

            $field.hide();
            //save ace div id to drupal wys storage 
            Drupal.wysiwyg.ace[params.field].aceDivId = aceDivId;
            //create a div with editor
            $field.after(aceDivHtml);
            
            var $aceDiv=$('#' + aceDivId);
            
            $aceDiv.height($field.height()).width('100%').css('position','relative');

            var editor=ace.edit(aceDivId);

            //add to editor value from textarea
            editor.getSession().setValue(jQuery('#' + params.field).val());

            //check and set setting and etc
            var commonParams = settings.buttons.common;
            var languageParams = settings.buttons.programming_languages;
            var themeParams = settings.buttons.theme_setting;
            
            editor.setSelectionStyle(commonParams.select_style ? "line" : "text");
    
            editor.setHighlightActiveLine(commonParams.highlight_active);
            
            editor.setShowInvisibles(commonParams.show_hidden);

            editor.renderer.setShowGutter(commonParams.show_gutter);
    
            editor.renderer.setShowPrintMargin(commonParams.show_print_margin);
    
            editor.setHighlightSelectedWord(commonParams.highlight_selected_word);
                
            editor.renderer.setHScrollBarAlwaysVisible(commonParams.show_hscroll);
            
            editor.getSession().setUseSoftTabs(commonParams.soft_tab);

            editor.setBehavioursEnabled(commonParams.enable_behaviours);

    
            //soft_wrap
            
            var session = editor.getSession();
            var renderer = editor.renderer;
            switch (commonParams.soft_wrap) {
                case "off":
                    session.setUseWrapMode(false);
                    renderer.setPrintMarginColumn(80);
                    break;
                case "40":
                    session.setUseWrapMode(true);
                    session.setWrapLimitRange(40, 40);
                    renderer.setPrintMarginColumn(40);
                    break;
                case "80":
                    session.setUseWrapMode(true);
                    session.setWrapLimitRange(80, 80);
                    renderer.setPrintMarginColumn(80);
                    break;
                case "free":
                    session.setUseWrapMode(true);
                    session.setWrapLimitRange(null, null);
                    renderer.setPrintMarginColumn(80);
                    break;
            }
            
            
            //theme
            
            var themePrefix='ace/theme/'
            var theme = editor.setTheme(themePrefix+themeParams.theme);
            
            
            //fontsize
            document.getElementById(aceDivId).style.fontSize=commonParams.fontsize;
            
            

            /*modes*/
            var modePrefix='ace/mode/';
            var currentModeName="javascript";
            var currentMode = require(modePrefix+currentModeName).Mode;
            
            editor.getSession().setMode(new currentMode());

            //after all actions save the editor
            Drupal.wysiwyg.ace[params.field].editor = editor;

            //var JavaScriptMode = require("ace/mode/javascript").Mode;
            //Drupal.wysiwyg.ace[params.field].getSession().setMode(new JavaScriptMode());
	}
};

    /**
     * Detach a single or all editors.
     *
     * See Drupal.wysiwyg.editor.detach.none() for a full desciption of this hook.
     */
    Drupal.wysiwyg.editor.detach.ace = function(context, params) {
	if(typeof params != 'undefined') {
		
            var $field=jQuery('#'+params.field);
            //put data from editor to textarea
            var editorData = Drupal.wysiwyg.ace[params.field].editor.getSession().getValue();
            //console.log(editorData);
            $field.val(editorData).show();
            //jQuery('#'+params.field).show();
            //alert(Drupal.wysiwyg.ace[params.field].editor.getSession().getValue());
            //delete the editor;


            delete Drupal.wysiwyg.ace[params.field].editor;
            $('#' + Drupal.wysiwyg.ace[params.field].aceDivId).remove();
                
	}
    };

})(jQuery);