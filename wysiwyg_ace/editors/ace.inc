<?php
/**
 * @file
 * Editor integration functions for ace.
 */

/**
 * Plugin implementation of hook_editor().
 */

function wysiwyg_ace_ace_editor() {
  $editor['ace'] = array(
    'title' => 'Ajax Code Editor',
    'vendor url' => 'http://ace.ajax.org',
    'download url' => 'https://github.com/ajaxorg/ace',
    'library path' => wysiwyg_get_path('ace').'/build/src',
    'root path' => wysiwyg_get_path('ace'),
    'libraries' => array(
      '' => array(
        'title' => 'ace',
        'files' => array('ace.js'),
      ),
    ),
    'version callback' => 'wysiwyg_ace_version',
    'themes callback' => 'wysiwyg_ace_themes',
    'settings callback' => 'wysiwyg_ace_settings',
    'load callback' => 'wysiwyg_ace_load',
    'plugin callback' => 'wysiwyg_ace_plugins',
    'versions' => array(
      '0.2.0' => array(
        'js files' => array('ace-0.2.js'),
        'css files' => array('ace-0.2.css'),
      ),
    ),
  );
  return $editor;
}

/**
 * Detect editor version.
 *
 * @param $editor
 *   An array containing editor properties as returned from hook_editor().
 *
 * @return
 *   The installed editor version.
 */
function wysiwyg_ace_version($editor) {
	$fp = $editor['root path'] . '/build/ChangeLog.txt';
	
        if (!file_exists($fp)) {
		return;
	}
	$fp = fopen($fp, 'r');
	$line = fgets($fp);
        
        $version=substr($line,(strpos($line,'Version')+strlen('Version')),strlen($line));
	
        //if (preg_match('@([0-9\.]+)$@', $line, $version)) {
        if($version){
		fclose($fp);
                //echo "qwe".trim($version)."qwe";
                return trim($version);
                
	}
	fclose($fp);
}

/**
 * Determine available editor themes or check/reset a given one.
 *
 * @param $editor
 *   A processed hook_editor() array of editor properties.
 * @param $profile
 *   A wysiwyg editor profile.
 *
 * @return
 *   An array of theme names. The first returned name should be the default
 *   theme name.
 */
function wysiwyg_ace_themes($editor, $profile) {
	$themes = array('default');
	//$file_list = scandir($editor['library path'].'/');
	//print_r($file_list);
        return $themes;
}

/**
 * Perform additional actions upon loading this editor.
 *
 * @param $editor
 *   A processed hook_editor() array of editor properties.
 * @param $library
 *   The internal library name (array key) to use.
 */
function wysiwyg_ace_load($editor, $library) {
        //FUUUUUUUUUUU
        $profiles = wysiwyg_profile_load_all();
        
        //find ace and finding all modes and themes
        $modes=array();
        $themes=array();
        foreach ($profiles as $profile_name => $profile) {
            if($profile->editor=='ace'){
                $modes = array_merge($modes,$profile->settings['buttons']['programming_languages']['modes']);
                $themes = array_merge($themes,(array)$profile->settings['buttons']['theme_setting']['theme']);
            }
        }
        //add ace srcitp
        drupal_add_js($editor['library path'] . '/ace.js',array('weight' => 1));
        //add all modes and themes
        foreach ($themes as $key => $value) {
            drupal_add_js($editor['library path'] . '/theme-'.$value.'.js',array('weight' => 2));
        }
        
        foreach ($modes as $key => $value) {
            if($value){
                drupal_add_js($editor['library path'] . '/mode-'.$value.'.js',array('weight' => 3));
            }
        }
        //echo "<pre>".print_r($themes,true)."</pre>";

}

/**
 * Return runtime editor settings for a given wysiwyg profile.
 *
 * @param $editor
 *   A processed hook_editor() array of editor properties.
 * @param $config
 *   An array containing wysiwyg editor profile settings.
 * @param $theme
 *   The name of a theme/GUI/skin to use.
 *
 * @return
 *   A settings array to be populated in
 *   Drupal.settings.wysiwyg.configs.{editor}
 */
function wysiwyg_ace_settings($editor, $config, $theme) {
	return $config;
}

/**
 * Return internal plugins for this editor; semi-implementation of hook_wysiwyg_plugin().
 */
function wysiwyg_ace_plugins($editor) {
    return array();
}